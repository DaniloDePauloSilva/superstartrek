package br.fsa.SuperStarTrek.JogoTerminal;

import java.text.ParseException;
import java.util.InputMismatchException;
import java.util.Scanner;

import br.fsa.SuperStarTrek.kernel.ConstantesObjetosJogo;
import br.fsa.SuperStarTrek.kernel.Kernel;
import br.fsa.SuperStarTrek.kernel.exception.DistanciaMaximaExcedidaException;
import br.fsa.SuperStarTrek.kernel.exception.EnergiaInsuficienteException;
import br.fsa.SuperStarTrek.kernel.exception.PhaserBloqueadoException;
import br.fsa.SuperStarTrek.kernel.exception.TorpedosInsuficientesException;

public class JogoTerminal {

	private static Kernel kernel = new Kernel(Kernel.NIVEL_JOGO_1);
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		boolean jogando = true;

		while (jogando) {
			System.out.println("Digite 1\tMedidores");
			System.out.println("Digite 2\tCarregar escudo");
			System.out.println("Digite 3\tScaneamento de curto alcance");
			System.out.println("Digite 4\tScaneamento de longo alcance");
			System.out.println("Digite 5\tNavegação - Impulso");
			System.out.println("Digite 6\tNavegação - Warp");
			System.out.println("Digite 7\tAtirar torpedo");
			System.out.println("Digite 8\tAtirar phaser");
			
			System.out.println();
			
			int option = sc.nextInt();

			switch (option) {
			case 1:
				visualizarMedidores();
				break;
			case 2:
				carregarEscudo();
				break;
			case 3:
				scaneamentoCurto();
				break;
			case 4:
				scaneamentoLongo();
				break;
			case 5:
				navegacaoImpulso();
				break;
			case 6:
				navegacaoWarp();
				break;
			case 7:
				atirarTorpedo();
				break;
			case 8:
				atirarPhaser();
			default:
				System.out.println("OPÇÃO INVÁLIDA");
				break;

			}

		}

		sc.close();

	}

	private static void atirarPhaser() {

		try 
		{
			System.out.println("---------------------------------------------------------");
			System.out.println("Energia restante: " + kernel.naveJogador.getNivelEnergia());
			
			System.out.println("Digite a quantidade de energia do disparo do phaser ou digite \"sair\" para voltar:");
			
			String saida = sc.next();
			
			if(saida.equals("sair"))
				return;
			
			int valor = Integer.parseInt(saida);
			
			if(!kernel.naveJogador.atirarPhaser(valor))
			{
				System.out.println("Não tem nenhum alien neste quadrante");
			}
		}
		catch(EnergiaInsuficienteException ex)
		{
			System.out.println("Energia Insuficiente");
		}
		catch(PhaserBloqueadoException ex)
		{
			System.out.println("O Tiro foi bloqueado");
		}
	}

	private static void carregarEscudo() {
		
		try 
		{
			System.out.println("---------------------------------------------------------");
			System.out.println("Energia restante: " + kernel.naveJogador.getNivelEnergia());
			System.out.println("Energia escudo: " + kernel.naveJogador.getEnergiaEscudo());
			
			System.out.println("Digite o valor a ser carregado ou digite \"sair\" para voltar:");
			
			String saida = sc.next();

			if(saida.equals("sair"))
				return;
			
			int valor = Integer.parseInt(saida);
			
			kernel.naveJogador.carregarEscudo(valor);
			
		}
		catch(EnergiaInsuficienteException ex)
		{
			System.out.println("Energia Insuficiente");
		}
		catch(InputMismatchException ex)
		{
			System.out.println("Valor invalido");
		}
		
	}

	private static void visualizarMedidores() 
	{
		kernel.visualizarMedidores();
	}

	private static void atirarTorpedo() {

		try 
		{
			String entrada = "";
			
			System.out.println("Digite a direção em graus(de 0 até 359) ou \"sair\" para retornar: ");
			entrada = sc.next();
			
			if(entrada.equals("sair"))
				return;

			Double graus =	Double.parseDouble(entrada);
			
			System.out.println("Digite a distancia ou  \"sair\" para retornar: ");
			entrada = sc.next();
			
			if(entrada.equals("sair"))
				return;
			
			Double distancia = Double.parseDouble(entrada);

			if(kernel.naveJogador.atirarTorpedo(graus, distancia)) 
			{
				System.out.println("-----------------------------------");
				System.out.println("         DESTRUIU UM ALIEN");
				System.out.println("-----------------------------------");
			}
			else 
			{
				System.out.println("-----------------------------------");
				System.out.println("         ERROU O TIRO");
				System.out.println("-----------------------------------");
			}
			
			
		}
		catch(TorpedosInsuficientesException ex)
		{
			System.out.println("TORPEDOS INSUFICIENTES");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	private static void navegacaoWarp() {

		try 
		{
			String entrada = "";
			System.out.println("Digite a direção em graus(de 0 até 359) ou \"sair\" para retornar: ");
			entrada = sc.next();
			
			if(entrada.equals("sair"))
				return;

			Double graus =	Double.parseDouble(entrada);
			
			System.out.println("Digite a distancia ou  \"sair\" para retornar: ");
			entrada = sc.next();
			
			if(entrada.equals("sair"))
				return;
			
			Double distancia = Double.parseDouble(entrada);

			kernel.naveJogador.warp(graus, distancia);
			
		} catch (EnergiaInsuficienteException ex) {
			System.out.println("Energia insuficiente");
		} catch (DistanciaMaximaExcedidaException ex) {
			System.out.println("Distância Máxima Excedida");
		}
		catch(InputMismatchException ex)
		{
			System.out.println("Valor invalido");
		}
	}

	private static void navegacaoImpulso() {

		try {
			String entrada = "";
			System.out.println("Digite a direção em graus(de 0 até 359) ou \"sair\" para retornar: ");
			entrada = sc.next();
			
			if(entrada.equals("sair"))
				return;

			Double graus =	Double.parseDouble(entrada);
			
			System.out.println("Digite a distancia ou  \"sair\" para retornar: ");
			entrada = sc.next();
			
			if(entrada.equals("sair"))
				return;
			
			Double distancia = Double.parseDouble(entrada);

			kernel.naveJogador.impulso(graus, distancia);
			
		} catch (DistanciaMaximaExcedidaException ex) {
			System.out.println("Distância Máxima Excedida");
		}
		catch(InputMismatchException ex)
		{
			System.out.println("Valor invalido");
		}

	}

	private static void scaneamentoLongo() {
		System.out.println("---------------------------------------------------------------------");
		imprimirDadosPosicionamento();
		kernel.scaneamentoDeTodosOsQuadrantes();
		System.out.println("---------------------------------------------------------------------");
	}

	private static void scaneamentoCurto() {
		System.out.println("---------------------------------------------------------------------");
		imprimirDadosPosicionamento();
		kernel.imprimirQuadranteJogador();
		System.out.println("---------------------------------------------------------------------");
	}

	private static void imprimirDadosPosicionamento() {
		int[] quadranteAtualJogador = kernel.naveJogador.retornaPosicaoQuadrante();
		int[] setorAtualJogador = kernel.naveJogador.retornaPosicaoSetor();

		
		System.out.println(
				"Quadrante atual do jogador: " + (quadranteAtualJogador[0] + 1) + "-" + (quadranteAtualJogador[1] + 1));
		System.out.println("Setor atual do jogador: " + (setorAtualJogador[0] + 1) + "-" + (setorAtualJogador[1] + 1));
	}

}
