package br.fsa.SuperStarTrek.JogoTerminal;

import java.util.Scanner;

import br.fsa.SuperStarTrek.kernel.Kernel;
import br.fsa.SuperStarTrek.kernel.exception.DistanciaMaximaExcedidaException;
import br.fsa.SuperStarTrek.kernel.exception.EnergiaInsuficienteException;

public class TesteNavegacao {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		Kernel kernel = new Kernel(Kernel.NIVEL_JOGO_1);
		
		int[] posicaoJogador = kernel.naveJogador.retornaPosicaoGlobal();
		int[] quadranteAtualJogador = kernel.naveJogador.retornaPosicaoQuadrante();
		int[] setorAtualJogador = kernel.naveJogador.retornaPosicaoSetor();
		
		System.out.println("Posicao atual do jogador: " + posicaoJogador[0] + "-" + posicaoJogador[1]);
		System.out.println("Quadrante atual do jogador: " + quadranteAtualJogador[0] + "-" + quadranteAtualJogador[1]);
		System.out.println("Setor atual do jogador: " + setorAtualJogador[0] + "-" + setorAtualJogador[1]);
		
		kernel.imprimirQuadranteJogador();
	//	kernel.imprimirGalaxia();
		while (true)
		{
			posicaoJogador = kernel.naveJogador.retornaPosicaoGlobal();
			quadranteAtualJogador = kernel.naveJogador.retornaPosicaoQuadrante();
			setorAtualJogador = kernel.naveJogador.retornaPosicaoSetor();
			System.out.println("-------------------------------------------------------");
			//kernel.imprimirQuadranteJogador();
			kernel.scaneamentoDeTodosOsQuadrantes();
			
			System.out.println("Posicao atual do jogador: " + posicaoJogador[0] + "-" + posicaoJogador[1]);
			System.out.println("Quadrante atual do jogador: " + quadranteAtualJogador[0] + "-" + quadranteAtualJogador[1]);
			System.out.println("Setor atual do jogador: " + setorAtualJogador[0] + "-" + setorAtualJogador[1]);
			
			System.out.println("Digite os graus: ");
			Double graus = sc.nextDouble();
			
			System.out.println("Digite a distancia: ");
			Double distancia = sc.nextDouble();
			
			try 
			{
				//kernel.naveJogador.impulso(graus, distancia);
				kernel.naveJogador.warp(graus, distancia);
			}
			catch(EnergiaInsuficienteException ex)
			{
				System.out.println("Energia insuficiente");
			}
			catch(DistanciaMaximaExcedidaException ex)
			{
				System.out.println("Distância Máxima Excedida");
			}
		}
	}

}
