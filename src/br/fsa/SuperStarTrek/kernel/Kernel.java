package br.fsa.SuperStarTrek.kernel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

import br.fsa.SuperStarTrek.kernel.exception.PhaserBloqueadoException;
import br.fsa.SuperStarTrek.kernel.model.Base;
import br.fsa.SuperStarTrek.kernel.model.Estrela;
import br.fsa.SuperStarTrek.kernel.model.NaveAlien;
import br.fsa.SuperStarTrek.kernel.model.NaveJogador;

public class Kernel {

	public int[][] universo;
	public NaveJogador naveJogador;
	public List<NaveAlien> navesAliens;
	
	public List<Estrela> estrelas;
	
	public List<Base> bases;
	
	private Random random;
	
	private Timer timerJogo;
	
	private int diasRestantes;
	
	public static final int QTDE_SETORES = 8;
	public static final int QTDE_QUADRANTES = 8;
	public static final int LARGURA_UNIVERSO = QTDE_SETORES * QTDE_QUADRANTES;
	
	public static final int QTDE_NAVES_ALIENS_NIVEL_1 = 20;
	public static final int QTDE_NAVES_ALIENS_NIVEL_2 = 25;
	public static final int QTDE_NAVES_ALIENS_NIVEL_3 = 30;
	
	public static final int NIVEL_JOGO_1 = 1;
	public static final int NIVEL_JOGO_2 = 2;
	public static final int NIVEL_JOGO_3 = 3;
	
	
	private int nivelJogo;
	
	
	public Kernel(int nivelJogo) {
		random = new Random();
		this.universo = new int[LARGURA_UNIVERSO][LARGURA_UNIVERSO];
		this.nivelJogo = nivelJogo;
		
		this.naveJogador = new NaveJogador(this);
		iniciar();
		
	};
	
	private void iniciar() {
		posicionarJogador();
		posicionarBase();
		posicionarEstrelas();
		posicionarAliens();
		this.setDiasRestantes(ConstantesObjetosJogo.DIAS_PARA_COMPLETAR_A_MISSAO);
		
		configurarTimerJogo();
		
	}


	

	private void configurarTimerJogo() {

		setTimerJogo(new Timer());
		
		getTimerJogo().scheduleAtFixedRate(new TimerTask() {
			
			@Override
			public void run() {
				
				Kernel.this.setDiasRestantes(Kernel.this.getDiasRestantes() - 1);
				
				if(Kernel.this.getDiasRestantes() == 0)
					Kernel.this.tempoDeJogoAcabou();
			}
		}, 30000, 30000);
		
	}

	private void posicionarJogador()
	{
		int linha = random.nextInt(LARGURA_UNIVERSO);
		int coluna = random.nextInt(LARGURA_UNIVERSO);
		
		this.naveJogador.setPosicao(linha, coluna);
	}
	
	private void posicionarAliens() 
	{
		switch(nivelJogo)
		{
			case NIVEL_JOGO_1:
				posicionarAliens(QTDE_NAVES_ALIENS_NIVEL_1);
				break;
				
			case NIVEL_JOGO_2:
				posicionarAliens(QTDE_NAVES_ALIENS_NIVEL_2);
				break;
				
			case NIVEL_JOGO_3:
				posicionarAliens(QTDE_NAVES_ALIENS_NIVEL_3);
				break;
		}
	}
	
	private void posicionarBase() 
	{
		this.limparBases();
		
		for(int i = 0; i < ConstantesObjetosJogo.QTDE_BASES; i++)
		{
			Base base = new Base(this, this.bases.size() + 1);
			
			int linha;
			int coluna;
			
			do 
			{
				linha = random.nextInt(LARGURA_UNIVERSO);
				coluna = random.nextInt(LARGURA_UNIVERSO);
			}
			while(reposicionarBase(linha, coluna));
			
			base.setPosicao(linha, coluna);
			bases.add(base);
		}
	}
	
	private void posicionarEstrelas() 
	{
		this.limparEstrelas();
		
		for(int i = 0; i < ConstantesObjetosJogo.QTDE_ESTRELAS; i++)
		{
			Estrela estrela = new Estrela(this, this.estrelas.size() + 1);
			
			int linha;
			int coluna;
			
			do 
			{
				linha = random.nextInt(LARGURA_UNIVERSO);
				coluna = random.nextInt(LARGURA_UNIVERSO);
			}
			while(reposicionarEstrela(linha, coluna));
			
			estrela.setPosicao(linha, coluna);
			estrelas.add(estrela);
		}
	}
	
	private void posicionarAliens(int qtdeAliens) 
	{
		this.limparAliens();
		
		for(int i = 0; i < qtdeAliens; i++)
		{
			NaveAlien alien = new NaveAlien(this, this.navesAliens.size() + 1);
			
			int linha;
			int coluna;
			
			do 
			{
				linha = random.nextInt(LARGURA_UNIVERSO);
				coluna = random.nextInt(LARGURA_UNIVERSO);
			}
			while(reposicionarAlien(linha, coluna));
			
			alien.setPosicao(linha, coluna);
			navesAliens.add(alien);
		}
	}
	
	private void limparBases() 
	{
		this.bases = new ArrayList<>();
	}
	
	private void limparEstrelas() 
	{
		this.estrelas = new ArrayList<>();
		
		for(int i = 0; i < this.universo.length; i++)
		{
			for(int j = 0; j < this.universo[0].length; j++)
			{
				universo[i][j] &= ~ConstantesObjetosJogo.ESTRELA;
			}
		}
	}

	private void limparAliens() {
		
		this.navesAliens = new ArrayList<>();

		for(int i = 0; i < universo.length; i++)
		{
			for(int j = 0; j < universo[0].length; j++)
			{
				universo[i][j] &= ~ConstantesObjetosJogo.NAVE_ALIEN;
			}
		}
	}
	
	private List<int[]> retornaPosicoesBases() 
	{
		return this.bases.stream().map(base -> base.getPosicaoAtual()).collect(Collectors.toList());
	}
	
	private List<int[]> retornaPosicoesEstrelas()
	{
		return this.estrelas.stream().map(estrela -> estrela.getPosicaoAtual()).collect(Collectors.toList());
	}

	private List<Integer[]> retornaPosicoesAliens()
	{
		List<Integer[]> retorno = new ArrayList<>();
		
		for(int i = 0; i < universo.length; i++)
		{
			for(int j = 0; j < universo[0].length; j++)
			{
				if(posicaoTemAlien(i, j))
					retorno.add(new Integer[] { i, j });
			}
		}
		
		return retorno;
	}
	
	public boolean posicaoTemAlien(int linha, int coluna) {

		return (universo[linha][coluna] & ConstantesObjetosJogo.NAVE_ALIEN) == ConstantesObjetosJogo.NAVE_ALIEN;
	}
	
	public boolean posicaoTemEstrela(int linha, int coluna)
	{
		return (universo[linha][coluna] & ConstantesObjetosJogo.ESTRELA) == ConstantesObjetosJogo.ESTRELA;
	}
	
	public boolean posicaoTemBase(int linha, int coluna)
	{
		return (universo[linha][coluna] & ConstantesObjetosJogo.BASE) == ConstantesObjetosJogo.BASE;
	}

//	private void posicionarAlien(IRegraPosicionarmentoAlien regra, int tipoObjetoAlien) 
//	{
//		int linha = random.nextInt(LARGURA_UNIVERSO);
//		int coluna = random.nextInt(LARGURA_UNIVERSO);
//		
//		if(regra.reposicionarAlien(linha, coluna))
//		{
//			posicionarAlien(regra, tipoObjetoAlien);
//			return;
//		}	
//			
//		universo[linha][coluna] |= tipoObjetoAlien;
//	}
	
	private boolean reposicionarBase(int linha, int coluna)
	{
		if(existeJogadorPerto(linha, coluna))
		{
			return true;
		}
		
		if(existeOutraBaseNoMesmoQuadrante(linha, coluna))
		{
			return true;
		}
		
//		if(existemOutrasBasesPerto(linha, coluna))
//		{
//			return true;
//		}
		
		return false;
		
	}

	

	private boolean reposicionarEstrela(int linha, int coluna)
	{
		if(existeJogadorPerto(linha, coluna))
		{
			return true;
		}
		
		if(existemOutrasBasesPerto(linha, coluna))
		{
			return true;
		}
		
		if(this.existemOutrasEstrelasPerto(linha, coluna))
		{
			return true;
		}
		
		return false;
	}
	
	private boolean reposicionarAlien(int linha, int coluna){
		
		if(existeJogadorPerto(linha, coluna))
		{
			return true;
		}	
		
		if(this.existemOutrasEstrelasPerto(linha, coluna))
		{
			return true;
		}
		
		if(this.existemOutrosAliensPerto(linha, coluna))
		{
			return true;
		}
		
		return false;
	}
	
	private boolean existeJogadorPerto(int linha, int coluna)
	{
		int[] posicaoJogador = naveJogador.retornaPosicaoGlobal();
		
		return (Math.abs(linha - posicaoJogador[0]) < 5 && Math.abs(coluna - posicaoJogador[1]) < 5);
	}
	
	private boolean quadranteJaFoiVisitado(int linha, int coluna) {
		
		List<int[]> quadrantesVisitados = naveJogador.quadrantesVisitados;
		
		for(int[] p : quadrantesVisitados)
		{
			if(p[0] == linha && p[1] == coluna)
				return true;
		}
		
		return false;
	}
	
	private boolean existemOutrasBasesPerto(int linha, int coluna) {
		
		List<int[]> posicoesBases = retornaPosicoesBases();
		
		for(int[] posicao : posicoesBases)
		{
			if(Math.abs(linha - posicao[0]) < 5 && Math.abs(coluna - posicao[1]) < 5)
				return true;
		}
		
		return false;
	}

	private boolean existeOutraBaseNoMesmoQuadrante(int linha, int coluna) {
		
		int[][] quadrante = retornaQuadranteDaPosicao(linha, coluna);
				
		for(int i = 0; i < quadrante.length; i++)
		{
			for(int j = 0; j < quadrante[0].length; j++)
			{
				if((quadrante[i][j] & ConstantesObjetosJogo.BASE) == ConstantesObjetosJogo.BASE)
				{
					return true;
				}
			}
		}
		
		return false;
	}

	private boolean existemOutrasEstrelasPerto(int linha, int coluna) 
	{
		List<int[]> posicoesEstrelas = retornaPosicoesEstrelas();
		
		for(int[] posicao : posicoesEstrelas)
		{
			if(Math.abs(linha - posicao[0]) < 5 && Math.abs(coluna - posicao[1]) < 5)
			{
				return true;
			}
		}
		
		return false;
	}
	
	private boolean existemOutrosAliensPerto(int linha, int coluna) {
		
		List<Integer[]> posicoesAliens = retornaPosicoesAliens();
		
		for(Integer[] posicao : posicoesAliens)
		{
			if(Math.abs(linha - posicao[0]) < 5 && Math.abs(coluna - posicao[1]) < 5)
			{
				return true;
			}
		}
		
		return false;
	}
	
	public int[][] retornaQuadranteDaPosicao(int linhaGlobal, int colunaGlobal)
	{
		int[][] quadrante = new int[QTDE_SETORES][QTDE_SETORES];
		
		int linha = linhaGlobal % QTDE_SETORES;
		int coluna = colunaGlobal % QTDE_SETORES;
		
		for(int i = linhaGlobal - linha, cont1 = 0; i < (linhaGlobal - linha) + QTDE_SETORES; i++, cont1++)
		{
			for(int j = colunaGlobal - coluna, cont2 = 0; j < (colunaGlobal - coluna) + QTDE_SETORES; j++, cont2++)
			{
				quadrante[cont1][cont2] = universo[i][j];
			}
		}
		
		return quadrante;
	}
	
	public void imprimirQuadranteJogador() 
	{
		int[] posicaoJogador = naveJogador.retornaPosicaoGlobal();
		int[][] quadrante = retornaQuadranteDaPosicao(posicaoJogador[0], posicaoJogador[1]);
		
		System.out.println(" \t1|\t2|\t3|\t4|\t5|\t6|\t7|\t8|");
		System.out.println(" \t--\t--\t--\t--\t--\t--\t--\t--");
		
		for(int i = 0; i < quadrante.length; i++)
		{
			System.out.print((i + 1) + "|\t");
			
			for(int j = 0; j < quadrante[0].length; j++)
			{
				if((quadrante[i][j] & ConstantesObjetosJogo.NAVE_JOGADOR) == ConstantesObjetosJogo.NAVE_JOGADOR)
				{
					System.out.print("J\t");
				}
				else if((quadrante[i][j] & ConstantesObjetosJogo.NAVE_ALIEN) == ConstantesObjetosJogo.NAVE_ALIEN)
				{
					System.out.print("A\t");
				}
				else if((quadrante[i][j] & ConstantesObjetosJogo.ESTRELA) == ConstantesObjetosJogo.ESTRELA)
				{
					System.out.print("E\t");
				}
				else if((quadrante[i][j] & ConstantesObjetosJogo.BASE) == ConstantesObjetosJogo.BASE)
				{
					System.out.print("B\t");
				}
				else 
				{
					System.out.print(" \t");
				}
				
				//System.out.print(quadrante[i][j] + "\t");
			}
			
			System.out.println();
		}
		
		System.out.println();
		System.out.println("J -> Jogador");
		System.out.println("A -> Alien");
		System.out.println("E -> Estrela");
		System.out.println("B -> BASE");
	}
	
//	public void imprimirQuadranteJogador() 
//	{
//		int[] posicaoJogador = naveJogador.retornaPosicaoGlobal();
//		
//		int linha = posicaoJogador[0] % QTDE_SETORES;
//		int coluna = posicaoJogador[1] % QTDE_SETORES;
//		
//		System.out.println(" \t1|\t2|\t3|\t4|\t5|\t6|\t7|\t8|");
//		System.out.println(" \t--\t--\t--\t--\t--\t--\t--\t--");
//		
//		for(int i = posicaoJogador[0] - linha; i < (posicaoJogador[0] - linha) + QTDE_SETORES; i++)
//		{
//			System.out.print(((i % 8) + 1) + "|\t");
//			
//			for(int j = posicaoJogador[1] - coluna; j < (posicaoJogador[1] - coluna) + QTDE_SETORES; j++)
//			{
//				System.out.print(universo[i][j] + "\t");
//			}
//			
//			System.out.println();
//		}
//		
//	}

	
	public void scaneamentoDeTodosOsQuadrantes() {
		int[] posicaoJogador = naveJogador.retornaPosicaoGlobal();
		System.out.println();
		System.out.println(" \t1    |\t2    |\t3    |\t4    |\t5    |\t6    |\t7    |\t8    |");
		System.out.println(" \t------\t------\t------\t------\t------\t------\t------\t------");
		
		
		for(int i = 0; i < QTDE_QUADRANTES; i++)
		{
			System.out.print((i + 1) + "|\t");
			
			for(int j = 0; j < QTDE_QUADRANTES; j++)
			{
				int qtdeAliens = 0;
				int qtdeBases = 0;
				int qtdeEstrelas = 0;
				
				for(int k = 0; k < QTDE_SETORES; k++)
				{
					for(int l = 0; l < QTDE_SETORES; l++)
					{
						int linha = (i * QTDE_QUADRANTES) + k;
						int coluna = (j * QTDE_QUADRANTES) + l;
						
						if(posicaoTemAlien(linha, coluna))
							qtdeAliens++;
						if(posicaoTemBase(linha, coluna))
							qtdeBases++;
						if(posicaoTemEstrela(linha, coluna))
							qtdeEstrelas++;
					}
				}
				
				if(quadranteJaFoiVisitado(i, j))
					System.out.print(qtdeAliens + " " + qtdeBases + " " + qtdeEstrelas + "\t");
				else
					System.out.print("     \t");
			}
			
			System.out.println();
		}
	}

	

	public boolean interrupcaoPercurso(int linha, int coluna) {
		
		if(linha >= universo.length || coluna >= universo[0].length || linha < 0 || coluna < 0)
			return true;
			
		if(posicaoTemAlien(linha, coluna))
			return true;
		
		if(posicaoTemEstrela(linha, coluna))
			return true;
		
		if(posicaoTemJogador(linha, coluna))
			return true;
		
		return false;
	}

	private boolean posicaoTemJogador(int linha, int coluna) {
		
		if((universo[linha][coluna] & ConstantesObjetosJogo.NAVE_JOGADOR) == ConstantesObjetosJogo.NAVE_JOGADOR)
			return true;
		
		return false;
	}

	public NaveAlien retornaAlienDaPosicao(int linha, int coluna)
	{
		for(NaveAlien n : navesAliens)
		{
			int[] posicao = n.retornaPosicaoGlobal();
			
			if(posicao[0] == linha && posicao[1] == coluna)
				return n;
		}
		
		return null;
	}

	public void removerNaveDestruida(NaveAlien n) {
		
		int[] pos = n.retornaPosicaoGlobal();
		
		this.universo[pos[0]][pos[1]] &= ~ConstantesObjetosJogo.NAVE_ALIEN;
		
		n.cancelarTimers();
		navesAliens.remove(n);
		
	}

	public int getQtdeAliensRestante() {

		return this.navesAliens.size();
	}
	
	public int[] retornaPosicaoQuadranteDoPonto(int linha, int coluna)
	{
		return new int[] { (int) Math.ceil(linha / 8), (int) Math.ceil(coluna / 8) };
	}

	public void visualizarMedidores() 
	{
		System.out.println("--------------------------------------------------------------");
		System.out.println("Helth: " + naveJogador.getNivelHealth() + " / " + ConstantesObjetosJogo.NIVEL_HEALTH_INICIAL);
		System.out.println("Energia restante: " + naveJogador.getNivelEnergia());
		System.out.println("Energia escudo: " + naveJogador.getEnergiaEscudo());
		System.out.println("Qtde. Torpedos: " + naveJogador.getQtdeTorpedos());
		System.out.println();
		System.out.println("Qtde. dias para terminar o jogo: " + getDiasRestantes());
		System.out.println("Qtde. Aliens Restantes.: " + getQtdeAliensRestante());
		System.out.println("--------------------------------------------------------------");
	}
	
	public void tempoDeJogoAcabou() 
	{
		System.out.println("---------------------------------------------------");
		System.out.println("TEMPO ACABOU - FIM DE JOGO");
		System.out.println("---------------------------------------------------");
		
		System.exit(0);
	}
	
	public void todosOsAliensForamDestruidos() 
	{
		System.out.println("---------------------------------------------------");
		System.out.println("TODOS OS INIMIGOS FORAM DESTRUÍDOS - FIM DE JOGO");
		System.out.println("---------------------------------------------------");
		
		System.exit(0);
	}

	public void jogadorDestruido() {

		System.out.println("------------------------------------");
		System.out.println("JOGADOR FOI DESTRUÍDO - FIM DE JOGO");
		System.out.println("------------------------------------");
		
		System.exit(0);
	}

	public Timer getTimerJogo() {
		return timerJogo;
	}

	public void setTimerJogo(Timer timerJogo) {
		this.timerJogo = timerJogo;
	}

	public int getDiasRestantes() {
		return diasRestantes;
	}

	public void setDiasRestantes(int diasRestantes) {
		this.diasRestantes = diasRestantes;
	}
	
}
