package br.fsa.SuperStarTrek.kernel.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.fsa.SuperStarTrek.kernel.ConstantesObjetosJogo;
import br.fsa.SuperStarTrek.kernel.Kernel;
import br.fsa.SuperStarTrek.kernel.exception.DistanciaMaximaExcedidaException;
import br.fsa.SuperStarTrek.kernel.exception.EnergiaInsuficienteException;
import br.fsa.SuperStarTrek.kernel.exception.PhaserBloqueadoException;
import br.fsa.SuperStarTrek.kernel.exception.TorpedosInsuficientesException;

public class NaveJogador implements ObjetoJogo {

	private Kernel kernel;
	private int[][] universo;

	private int nivelEnergia;
	private int energiaEscudo;
	private int energiaBancoFaser;
	private int qtdeTorpedos;
	
	public List<int[]> quadrantesVisitados;
	private int nivelHealth;

	public NaveJogador(Kernel kernel) {
		this.kernel = kernel;
		this.universo = kernel.universo;
		this.nivelEnergia = ConstantesObjetosJogo.ENERGIA_INICIAL_JOGADOR;
		this.setNivelHealth(ConstantesObjetosJogo.NIVEL_HEALTH_INICIAL); 
		this.setEnergiaEscudo(0);
		this.setEnergiaBancoFaser(0);
		this.setQtdeTorpedos(ConstantesObjetosJogo.QTDE_INICIAL_TORPEDOS_JOGADOR);
		
		quadrantesVisitados = new ArrayList<>();
	}
	
	public void carregarEscudo(int valor) throws EnergiaInsuficienteException
	{
		
		if(valor > nivelEnergia)
			throw new EnergiaInsuficienteException();
		
		this.nivelEnergia -= valor;
		this.energiaEscudo += valor;
	}
	
	public void subtrairEnergia(int qtde) throws EnergiaInsuficienteException 
	{
		if(qtde > nivelEnergia)
			throw new EnergiaInsuficienteException();
		
		nivelEnergia -= qtde;
	}

	public int[] retornaPosicaoGlobal() {
		for (int i = 0; i < universo.length; i++) {
			for (int j = 0; j < universo[0].length; j++) {
				if ((universo[i][j] & ConstantesObjetosJogo.NAVE_JOGADOR) == ConstantesObjetosJogo.NAVE_JOGADOR) {
					return new int[] { i, j };
				}
			}
		}

		return new int[] { -1, -1 };
	}

	public int[] retornaPosicaoQuadrante() {
		int[] posicaoJogador = retornaPosicaoGlobal();

		return new int[] { (int) Math.ceil(posicaoJogador[0] / 8), (int) Math.ceil(posicaoJogador[1] / 8) };
	}

	public int[] retornaPosicaoSetor() {

		int[] posicaoSetor = new int[2];
		int[] posicaoAtualJogador = retornaPosicaoGlobal();

		posicaoSetor[0] = posicaoAtualJogador[0] % kernel.QTDE_SETORES;
		posicaoSetor[1] = posicaoAtualJogador[1] % kernel.QTDE_SETORES;

		return posicaoSetor;
	}

	public void setPosicao(int linha, int coluna) {

		int[] posicaoAntiga = retornaPosicaoGlobal();
		
		if (posicaoAntiga[0] != -1) {
			this.universo[posicaoAntiga[0]][posicaoAntiga[1]] &= ~ConstantesObjetosJogo.NAVE_JOGADOR;
		}

		this.universo[linha][coluna] |= ConstantesObjetosJogo.NAVE_JOGADOR;
		
		if(kernel.posicaoTemBase(linha, coluna))
		{
			System.out.println("---------------------------------------------------------");
			System.out.println("Jogador está na base, recarregando energia e torpedos");
			System.out.println("---------------------------------------------------------");
			
			this.recarregarNaBase();
		}
		
		atualizarQuadrantesVisitados(linha, coluna);
	}

	private void recarregarNaBase() {
		
		this.nivelEnergia = ConstantesObjetosJogo.ENERGIA_INICIAL_JOGADOR;
		this.setQtdeTorpedos(ConstantesObjetosJogo.QTDE_INICIAL_TORPEDOS_JOGADOR);
	}

	private void atualizarQuadrantesVisitados(int linha, int coluna) {

		int[] quadranteAtual = retornaPosicaoQuadrante();

		boolean adiciona = true;
		
		for(int[] p : quadrantesVisitados)
		{
			if(p[0] == linha && p[1] == coluna)
			{
				adiciona = false;
				break;
			}
		}
		
		if(adiciona)
			quadrantesVisitados.add(quadranteAtual);
		
		
//		System.out.print("quadrantesVisitados: ");
//		for(int[] p : quadrantesVisitados)
//		{
//			System.out.print("[" + p[0] + ", " + p[1] + "], ");
//		}
//		System.out.println();
	}

	public void transferirEnergiaParaOEscudo(int qtdeEnergia) throws Exception {
		if (qtdeEnergia > this.nivelEnergia) {
			throw new EnergiaInsuficienteException();
		}

		this.nivelEnergia -= qtdeEnergia;
		this.setEnergiaEscudo(this.getEnergiaEscudo() + qtdeEnergia);

	}

	private boolean existeAlienNoQuadrante() {
		
		int posicao[] = retornaPosicaoGlobal();
		
		int[][] quadrante = kernel.retornaQuadranteDaPosicao(posicao[0], posicao[1]);
		
		for(int i = 0; i < quadrante.length; i++)
		{
			for(int j = 0; j < quadrante[0].length; j++)
			{
				if((quadrante[i][j] & ConstantesObjetosJogo.NAVE_ALIEN) == ConstantesObjetosJogo.NAVE_ALIEN)
					return true;
			}
		}
		
		return false;
	}
	
	private NaveAlien retornaAlienQuadrante() {

		int[] posicaoJogador = retornaPosicaoGlobal();
		
		int linhaGlobal = posicaoJogador[0];
		int colunaGlobal = posicaoJogador[1];
		
		int linha = linhaGlobal % Kernel.QTDE_SETORES;
		int coluna = colunaGlobal % Kernel.QTDE_SETORES;
		
		for(int i = linhaGlobal - linha; i < (linhaGlobal - linha) + Kernel.QTDE_SETORES; i++)
		{
			for(int j = colunaGlobal - coluna; j < (colunaGlobal - coluna) + Kernel.QTDE_SETORES; j++)
			{
				if((universo[i][j] & ConstantesObjetosJogo.NAVE_ALIEN) == ConstantesObjetosJogo.NAVE_ALIEN)
				{
					for(NaveAlien n : kernel.navesAliens)
					{
						int[] pos = n.retornaPosicaoGlobal();
						
						if(pos[0] == i && pos[1] == j)
							return n;
					}
				}
			}
		}
		
		return null;
	}
	
	public boolean atirarPhaser(int energia) throws EnergiaInsuficienteException, PhaserBloqueadoException
	{
		if(getNivelEnergia() < energia)
			throw new EnergiaInsuficienteException();
		else if(!existeAlienNoQuadrante())
			return false;
		
		this.subtrairEnergia(energia);
		
		int[] posicaoJogador = retornaPosicaoGlobal();
		NaveAlien nave = retornaAlienQuadrante();
		int[] posicaoAlien = nave.retornaPosicaoGlobal();
		
		int xi = posicaoJogador[1];
		int yi = posicaoJogador[0];
		int x = xi;
		int y = yi;
		int xf = posicaoAlien[1];
		int yf = posicaoAlien[0];
		
		int incX = 1;
		int incY = 1;

		float deltaX = xf - xi;
		float deltaY = yf - yi;

		float coefA = (deltaY / deltaX);
		float coefB = yi - (coefA * xi);

		if (xi > xf) {
			incX = -1;
		}

		if (yi > yf) {
			incY = -1;
		}

		if (deltaX < 0) {
			deltaX = -deltaX;
		}

		if (deltaY < 0) {
			deltaY = -deltaY;
		}

		if (deltaX >= deltaY) {
			while (x != xf) {

				x += incX;

				float dy = (coefA * x) + coefB;

				dy = Math.abs(dy - y);

				if (dy > 0.5) {
					y += incY;
				}
				
				if(kernel.posicaoTemEstrela(y, x))
				{
					throw new PhaserBloqueadoException();
				}
			}
		} else {
			while (y != yf) {

				y += incY;

				float dx = (y - coefB) / coefA;

				dx = Math.abs(dx - x);

				if (dx > 0.5) {
					x += incX;
				}

				if(kernel.posicaoTemEstrela(y, x))
				{
					throw new PhaserBloqueadoException();
				}

			}
		}
		
		System.out.println("--------------------------------");
		System.out.println("Atingiu um alien");
		System.out.println("--------------------------------");
		nave.subtrairEnergia(energia);
		return true;
	}



	public boolean atirarTorpedo(Double graus, Double distancia) throws Exception {
		if (getQtdeTorpedos() == 0)
			throw new TorpedosInsuficientesException();
		else
			setQtdeTorpedos(getQtdeTorpedos() - 1);

		int[] posicaoJogador = retornaPosicaoGlobal();
		Double grausRad = Math.toRadians(graus - 90);

		int xi = posicaoJogador[1];
		int yi = posicaoJogador[0];

		int x = xi;
		int y = yi;

		int xf = xi + (int) Math.round(Math.cos(grausRad) * distancia);
		int yf = yi + (int) Math.round(Math.sin(grausRad) * distancia);

		int incX = 1;
		int incY = 1;

		float deltaX = xf - xi;
		float deltaY = yf - yi;

		float coefA = (deltaY / deltaX);
		float coefB = yi - (coefA * xi);

		if (xi > xf) {
			incX = -1;
		}

		if (yi > yf) {
			incY = -1;
		}

		if (deltaX < 0) {
			deltaX = -deltaX;
		}

		if (deltaY < 0) {
			deltaY = -deltaY;
		}

		if (deltaX >= deltaY) {
			while (x != xf) {

				x += incX;

				float dy = (coefA * x) + coefB;

				dy = Math.abs(dy - y);

				if (dy > 0.5) {
					y += incY;
				}

				if (kernel.posicaoTemAlien(y, x)) {
					NaveAlien n = kernel.retornaAlienDaPosicao(y, x);
					kernel.removerNaveDestruida(n);
					return true;
				}
			}
		} else {
			while (y != yf) {

				y += incY;

				float dx = (y - coefB) / coefA;

				dx = Math.abs(dx - x);

				if (dx > 0.5) {
					x += incX;
				}

				if (kernel.posicaoTemAlien(y, x)) {
					NaveAlien n = kernel.retornaAlienDaPosicao(y, x);
					kernel.removerNaveDestruida(n);
					return true;
				}

			}
		}

		return false;
	}

	private boolean quadranteVisitado(List<int[]> quadrantesViajados, int linha, int coluna)
	{
		for(int[] p : quadrantesViajados)
		{
			int[] quadrantePonto = kernel.retornaPosicaoQuadranteDoPonto(linha, coluna);
			
			if(quadrantePonto[0] == p[0] && quadrantePonto[1] == p[1])
			{
				return true;
			}
		}
		
		return false;
	}
	
	public void warp(Double graus, Double distancia) throws DistanciaMaximaExcedidaException, EnergiaInsuficienteException
	{
		if(distancia >= kernel.QTDE_QUADRANTES)
			throw new DistanciaMaximaExcedidaException();
		
		int [] posicaoJogador = retornaPosicaoGlobal();
		Double grausRad = Math.toRadians(graus - 90);
		
		List<int[]> quadrantesViajados = new ArrayList<>();
		
		quadrantesViajados.add(retornaPosicaoQuadrante());
		
		int xi = posicaoJogador[1];
		int yi = posicaoJogador[0];
		int x = xi;
		int y = yi;

		int xf = xi + ((int) Math.round(Math.cos(grausRad) * distancia) * 8);
		int yf = yi + ((int) Math.round(Math.sin(grausRad) * distancia) * 8);

		int incX = 1;
		int incY = 1;

		float deltaX = xf - xi;
		float deltaY = yf - yi;

		float coefA = (deltaY / deltaX);
		float coefB = yi - (coefA * xi);

		if (xi > xf) {
			incX = -1;
		}

		if (yi > yf) {
			incY = -1;
		}

		if (deltaX < 0) {
			deltaX = -deltaX;
		}

		if (deltaY < 0) {
			deltaY = -deltaY;
		}
		
		if (deltaX >= deltaY) {
			while (x != xf) {

				int xAntigo = x;
				int yAntigo = y;

				x += incX;

				float dy = (coefA * x) + coefB;

				dy = Math.abs(dy - y);

				if (dy > 0.5) {
					y += incY;
				}
				
				if(!quadranteVisitado(quadrantesVisitados, y, x))
				{
					quadrantesVisitados.add(kernel.retornaPosicaoQuadranteDoPonto(y, x));
					this.subtrairEnergia(100);
				}

				if (kernel.interrupcaoPercurso(y, x)) {
					this.setPosicao(yAntigo, xAntigo);
					return;
				}
			}
		} else {
			while (y != yf) {

				int xAnterior = x;
				int yAnterior = y;

				y += incY;

				float dx = (y - coefB) / coefA;

				dx = Math.abs(dx - x);

				if (dx > 0.5) {
					x += incX;
				}
				
				if(!quadranteVisitado(quadrantesVisitados, y, x))
				{
					quadrantesVisitados.add(kernel.retornaPosicaoQuadranteDoPonto(y, x));
					this.subtrairEnergia(100);
				}

				if (kernel.interrupcaoPercurso(y, x)) {
					System.out.println("Percurso tem um obstáculo!!!!!!!");
					setPosicao(yAnterior, xAnterior);
					return;
				}

			}
		}

		setPosicao(yf, xf);
		
	}
	
	public void impulso(Double graus, Double distancia) throws DistanciaMaximaExcedidaException 
	{
		if(distancia >= kernel.QTDE_SETORES)
			throw new DistanciaMaximaExcedidaException();
		
		int[] posicaoJogador = retornaPosicaoGlobal();
		Double grausRad = Math.toRadians(graus - 90);

		int xi = posicaoJogador[1];
		int yi = posicaoJogador[0];

		int x = xi;
		int y = yi;

		int xf = xi + (int) Math.round(Math.cos(grausRad) * distancia);
		int yf = yi + (int) Math.round(Math.sin(grausRad) * distancia);

		int incX = 1;
		int incY = 1;

		float deltaX = xf - xi;
		float deltaY = yf - yi;

		float coefA = (deltaY / deltaX);
		float coefB = yi - (coefA * xi);

		if (xi > xf) {
			incX = -1;
		}

		if (yi > yf) {
			incY = -1;
		}

		if (deltaX < 0) {
			deltaX = -deltaX;
		}

		if (deltaY < 0) {
			deltaY = -deltaY;
		}

		if (deltaX >= deltaY) {
			while (x != xf) {

				int xAntigo = x;
				int yAntigo = y;

				x += incX;

				float dy = (coefA * x) + coefB;
				dy = Math.abs(dy - y);


				if (dy > 0.5) {
					y += incY;
				}
				
				if (kernel.interrupcaoPercurso(y, x)) {
					this.setPosicao(yAntigo, xAntigo);
					return;
				}
			}
		} else {
			while (y != yf) {

				int xAnterior = x;
				int yAnterior = y;

				y += incY;

				float dx = (y - coefB) / coefA;

				dx = Math.abs(dx - x);

				if (dx > 0.5) {
					x += incX;
				}

				if (kernel.interrupcaoPercurso(y, x)) {
					setPosicao(yAnterior, xAnterior);
					return;
				}

			}
		}

		setPosicao(yf, xf);
	}

	public int getNivelEnergia() {
		return this.nivelEnergia;
	}

	public int getEnergiaEscudo() {
		return energiaEscudo;
	}

	public void setEnergiaEscudo(int energiaEscudo) {
		this.energiaEscudo = energiaEscudo;
	}

	public int getEnergiaBancoFaser() {
		return energiaBancoFaser;
	}

	public void setEnergiaBancoFaser(int energiaBancoFaser) {
		this.energiaBancoFaser = energiaBancoFaser;
	}

	public int getQtdeTorpedos() {
		return qtdeTorpedos;
	}

	public void setQtdeTorpedos(int qtdeTorpedos) {
		this.qtdeTorpedos = qtdeTorpedos;
	}

	public int getNivelHealth() {
		return nivelHealth;
	}

	public void setNivelHealth(int nivelHealth) {
		this.nivelHealth = nivelHealth;
	}

	public void naveAtingidaPorAlien(int energiaTiro) {
		
		if(energiaTiro > energiaEscudo)
		{
			int energiaSobraTiro = energiaTiro - (energiaEscudo > 0 ? energiaEscudo : 0);
			this.energiaEscudo = 0;
			this.nivelHealth -= energiaSobraTiro;
		}
		else 
		{
			this.energiaEscudo -= energiaTiro;
		}
		
		if(this.nivelHealth <= 0)
		{
			kernel.jogadorDestruido();
		}
		
	}

	
	
	

}
