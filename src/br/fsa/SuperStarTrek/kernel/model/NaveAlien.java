package br.fsa.SuperStarTrek.kernel.model;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import br.fsa.SuperStarTrek.kernel.ConstantesObjetosJogo;
import br.fsa.SuperStarTrek.kernel.Kernel;
import br.fsa.SuperStarTrek.kernel.exception.PhaserBloqueadoException;

public class NaveAlien implements ObjetoJogo{

	private int idNave;
	private Kernel kernel;
	private int[][] universo;
	private int[] posicaoAtual;
	private int health;
	private Timer timerMovimentacao;
	private Timer timerTirosAliens;
	
	public NaveAlien(Kernel kernel, int idNave) {
		this.kernel = kernel;
		this.universo = kernel.universo;
		this.idNave = idNave;
		this.posicaoAtual = new int[] { -1, -1 };
		helthInicial();
		
		configurarTimerMovimentacao();
		configurarTimerTiros();
	}
	
	public void cancelarTimers() {
		
		try 
		{
			timerMovimentacao.cancel();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		try 
		{
			timerTirosAliens.cancel();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}

	private void configurarTimerMovimentacao() 
	{
		Random rd = new Random();
		int interval = 10000 + rd.nextInt(50000);
		
		if(timerMovimentacao == null)
			timerMovimentacao = new Timer();
		
		timerMovimentacao.schedule(new TimerTask() {
			
			@Override
			public void run() {
				
				NaveAlien.this.impulso();
				
				NaveAlien.this.configurarTimerMovimentacao();
			}
		}, interval);
	}
	
	protected void impulso() {

		Random rd = new Random();
		int[] posicaoAlien = retornaPosicaoGlobal();
		Double distancia = 1.0;
		Double graus = (double) rd.nextInt(360);
		Double grausRad = Math.toRadians(graus);
		
		int xi = posicaoAlien[1];
		int yi = posicaoAlien[0];
		
		int x = xi;
		int y = yi;
		
		int xf = xi + (int) Math.round(Math.cos(grausRad) * distancia);
		int yf = yi + (int) Math.round(Math.sin(grausRad) * distancia);

		int incX = 1;
		int incY = 1;

		float deltaX = xf - xi;
		float deltaY = yf - yi;

		float coefA = (deltaY / deltaX);
		float coefB = yi - (coefA * xi);

		if (xi > xf) {
			incX = -1;
		}

		if (yi > yf) {
			incY = -1;
		}

		if (deltaX < 0) {
			deltaX = -deltaX;
		}

		if (deltaY < 0) {
			deltaY = -deltaY;
		}

		if (deltaX >= deltaY) {
			while (x != xf) {

				int xAntigo = x;
				int yAntigo = y;

				x += incX;

				float dy = (coefA * x) + coefB;
				dy = Math.abs(dy - y);


				if (dy > 0.5) {
					y += incY;
				}
				
				if (kernel.interrupcaoPercurso(y, x)) {
					this.setPosicao(yAntigo, xAntigo);
					return;
				}
			}
		} else {
			while (y != yf) {

				int xAnterior = x;
				int yAnterior = y;

				y += incY;

				float dx = (y - coefB) / coefA;

				dx = Math.abs(dx - x);

				if (dx > 0.5) {
					x += incX;
				}

				if (kernel.interrupcaoPercurso(y, x)) {
					setPosicao(yAnterior, xAnterior);
					return;
				}

			}
		}

		setPosicao(yf, xf);
		
	}

	private void configurarTimerTiros() {

		Random rd = new Random();
		int delay = 10000 + rd.nextInt(10000);
		int interval = 20000;
		
		timerTirosAliens = new Timer();
				
		timerTirosAliens.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() 
			{
				int[] quadranteJogador = NaveAlien.this.kernel.naveJogador.retornaPosicaoQuadrante();
				
				int[] posicaoNave = NaveAlien.this.retornaPosicaoGlobal();
				int[] quadranteNave = NaveAlien.this.kernel.retornaPosicaoQuadranteDoPonto(posicaoNave[0], posicaoNave[1]);
				
				if(quadranteJogador[0] == quadranteNave[0] && quadranteJogador[1] == quadranteNave[1])
				{
					try 
					{
						NaveAlien.this.atirarNoJogador();
					}
					catch(PhaserBloqueadoException ex)
					{
						System.out.println("Tiro da nave id: " + NaveAlien.this.getIdNave() + " bloqueado por uma estrela");
					}
				}
			}
			
		}, delay, interval);
		
	}

	private void helthInicial() {
		
		Random rd = new Random();
		
		setHealth(150 + rd.nextInt(150));
		
	}

	@Override
	public int[] retornaPosicaoQuadrante() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] retornaPosicaoGlobal() {
		return this.posicaoAtual.clone();
	}

	@Override
	public int[] retornaPosicaoSetor() {
		// TODO Auto-generated method stub
		return null;
	}

	public int getIdNave() {
		return idNave;
	}

	public void setIdNave(int idNave) {
		this.idNave = idNave;
	}

	@Override
	public void setPosicao(int linha, int coluna) {
		
		int[] posicaoAntiga = retornaPosicaoGlobal();
		
		if(posicaoAntiga[0] != -1)
		{
			universo[posicaoAntiga[0]][posicaoAntiga[1]] &= ~ConstantesObjetosJogo.NAVE_ALIEN;
		}
		
		this.posicaoAtual[0] = linha;
		this.posicaoAtual[1] = coluna;
		
		this.universo[linha][coluna] |= ConstantesObjetosJogo.NAVE_ALIEN;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idNave;
		result = prime * result + ((kernel == null) ? 0 : kernel.hashCode());
		result = prime * result + Arrays.deepHashCode(universo);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NaveAlien other = (NaveAlien) obj;
		if (idNave != other.idNave)
			return false;
		if (kernel == null) {
			if (other.kernel != null)
				return false;
		} else if (!kernel.equals(other.kernel))
			return false;
		if (!Arrays.deepEquals(universo, other.universo))
			return false;
		return true;
	}

	public void subtrairEnergia(int energia) {
		
		this.health -= energia;
		
		if(health <= 0)
		{
			System.out.println("----------------------------");
			System.out.println("ALIEN DESTRUÍDO");
			System.out.println("----------------------------");
			
			kernel.removerNaveDestruida(this);
		}
		
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public void atirarNoJogador() throws PhaserBloqueadoException {
		Random rd = new Random();
		
		int energiaTiro = 100 + rd.nextInt(100); 
		
		int[] posicaoNaveJogador = kernel.naveJogador.retornaPosicaoGlobal();
		int[] posicaoAlien = retornaPosicaoGlobal();
		
		int xi = posicaoAlien[1];
		int yi = posicaoAlien[0];
		int x = xi;
		int y = yi;
		int xf = posicaoNaveJogador[1];
		int yf = posicaoNaveJogador[0];
		
		int incX = 1;
		int incY = 1;
		
		float deltaX = xf - xi;
		float deltaY = yf - yi;

		float coefA = (deltaY / deltaX);
		float coefB = yi - (coefA * xi);

		if (xi > xf) {
			incX = -1;
		}

		if (yi > yf) {
			incY = -1;
		}

		if (deltaX < 0) {
			deltaX = -deltaX;
		}

		if (deltaY < 0) {
			deltaY = -deltaY;
		}

		if (deltaX >= deltaY) {
			while (x != xf) {

				x += incX;

				float dy = (coefA * x) + coefB;

				dy = Math.abs(dy - y);

				if (dy > 0.5) {
					y += incY;
				}
				
				if(kernel.posicaoTemEstrela(y, x))
				{
					throw new PhaserBloqueadoException();
				}
			}
		} else {
			while (y != yf) {

				y += incY;

				float dx = (y - coefB) / coefA;

				dx = Math.abs(dx - x);

				if (dx > 0.5) {
					x += incX;
				}

				if(kernel.posicaoTemEstrela(y, x))
				{
					throw new PhaserBloqueadoException();
				}

			}
		}
		
		System.out.println("--------------------------------");
		System.out.println("Nave do Jogador foi atingida");
		System.out.println("--------------------------------");
		
		kernel.naveJogador.naveAtingidaPorAlien(energiaTiro);
		
	}

	
	
	

}
