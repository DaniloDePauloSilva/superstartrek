package br.fsa.SuperStarTrek.kernel.model;

import br.fsa.SuperStarTrek.kernel.ConstantesObjetosJogo;
import br.fsa.SuperStarTrek.kernel.Kernel;

public class Base implements ObjetoJogo {
	
	private Kernel kernel;
	private int idBase;
	private int[][] universo;
	
	private int[] posicaoAtual;

	public Base(Kernel kernel, int idBase)
	{
		this.kernel = kernel;
		this.universo = kernel.universo;
		this.idBase = idBase;
		
		this.posicaoAtual = new int[] { -1, -1 };
	}

	@Override
	public int[] retornaPosicaoQuadrante() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] retornaPosicaoGlobal() {

		return this.posicaoAtual.clone();
	}

	@Override
	public int[] retornaPosicaoSetor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPosicao(int linha, int coluna) {
		
		int[] posicaoAntiga = retornaPosicaoGlobal();
		
		if(posicaoAntiga[0] != -1)
		{
			universo[posicaoAntiga[0]][posicaoAntiga[1]] &= ~ConstantesObjetosJogo.BASE;
		}
		
		this.posicaoAtual[0] = linha;
		this.posicaoAtual[1] = coluna;
		
		this.universo[linha][coluna] |= ConstantesObjetosJogo.BASE;
	}

	public int[] getPosicaoAtual() {
		return posicaoAtual;
	}

	public void setPosicaoAtual(int[] posicaoAtual) {
		this.posicaoAtual = posicaoAtual;
	}

}
