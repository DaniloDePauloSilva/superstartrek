package br.fsa.SuperStarTrek.kernel.model;

public interface ObjetoJogo {
	
	int[] retornaPosicaoQuadrante();
	int[] retornaPosicaoGlobal();
	int[] retornaPosicaoSetor();
	
	void setPosicao(int linha, int coluna);
	

}
