package br.fsa.SuperStarTrek.kernel.model;

import br.fsa.SuperStarTrek.kernel.ConstantesObjetosJogo;
import br.fsa.SuperStarTrek.kernel.Kernel;

public class Estrela implements ObjetoJogo{

	private Kernel kernel;
	private int[][] universo;
	
	private int[] posicaoAtual;
	private int idEstrela;

	public Estrela(Kernel kernel, int idEstrela)
	{
		this.kernel = kernel;
		this.universo = kernel.universo;
		this.idEstrela = idEstrela;
		this.setPosicaoAtual(new int[] { -1, -1 });
	}
	
	@Override
	public int[] retornaPosicaoQuadrante() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] retornaPosicaoGlobal() {
		
		return this.getPosicaoAtual().clone();
	}

	@Override
	public int[] retornaPosicaoSetor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPosicao(int linha, int coluna) {
		
		int[] posicaoAntiga = this.retornaPosicaoGlobal();
		
		if(posicaoAntiga[0] != -1)
		{
			this.universo[posicaoAntiga[0]][posicaoAntiga[1]] &= ~ConstantesObjetosJogo.ESTRELA;
		}
		
		this.getPosicaoAtual()[0] = linha;
		this.getPosicaoAtual()[1] = coluna;
		
		this.universo[linha][coluna] |= ConstantesObjetosJogo.ESTRELA;
	}

	public int[] getPosicaoAtual() {
		return posicaoAtual;
	}

	public void setPosicaoAtual(int[] posicaoAtual) {
		this.posicaoAtual = posicaoAtual;
	}

}
