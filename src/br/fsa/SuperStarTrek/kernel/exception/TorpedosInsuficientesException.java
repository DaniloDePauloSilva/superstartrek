package br.fsa.SuperStarTrek.kernel.exception;

public class TorpedosInsuficientesException extends Exception {

	public TorpedosInsuficientesException() 
	{
		super();
	}
	
	public TorpedosInsuficientesException(String msg)
	{
		super(msg);
	}
}
