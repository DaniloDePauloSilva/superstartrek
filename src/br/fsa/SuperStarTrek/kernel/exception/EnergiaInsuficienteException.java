package br.fsa.SuperStarTrek.kernel.exception;

public class EnergiaInsuficienteException extends Exception {
	
	public EnergiaInsuficienteException() 
	{
		super();
	}
	
	public EnergiaInsuficienteException(String msg)
	{
		super(msg);
	}

}
