package br.fsa.SuperStarTrek.kernel;

public class ConstantesObjetosJogo {
	public static final int NAVE_JOGADOR = 1 << 0;
	public static final int NAVE_ALIEN = 1 << 1;
	public static final int ESTRELA = 1 << 2;
	public static final int BASE = 1 << 3;
	
	public static final int ENERGIA_INICIAL_JOGADOR = 3000;
	public static final int QTDE_INICIAL_TORPEDOS_JOGADOR = 10;
	
	public static final int QTDE_ESTRELAS = 10;
	public static final int QTDE_BASES = 3;
	public static final int NIVEL_HEALTH_INICIAL = 1000;
	public static final int DIAS_PARA_COMPLETAR_A_MISSAO = 25;
	
	
}
